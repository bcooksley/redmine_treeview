module ProjectsHelper
  def render_projects_tree(projects)
    member_projects = User.current.projects.all
    children = {}
    for project in projects
      children[ project.parent_id ] ||= []
      children[ project.parent_id ] << project
    end

    ancestors = []
    output = ""
    for project in projects
      rowid = classes = spanicon = openonclick = ""
      if( !children[project.id].nil? )
        classes = "closed parent #{cycle("odd", "even")}"
        rowid = "id=\"#{project.id.to_s}span\""
        openonclick = "onclick=\"showHide('#{project.id.to_s}','#{project.id.to_s}span')\""
        spanicon = "<span #{openonclick} class=\"expander\">&nbsp; </span>"
      else
        classes = "child"
      end
      if( project.parent_id == nil )
        ancestors.clear
        ancestors << project.id
      else
        while (ancestors.any? && !(project.parent_id == ancestors.last))
          ancestors.pop
        end
        if( !ancestors.include?( project.parent_id ) )
          prvclasses = "closed show parent #{cycle("odd", "even")} #{ancestors.join(' ')}"
          openonclick = "onclick=\"showHide('"+project.parent_id.to_s+"','"+project.parent_id.to_s+"span')\""
          output << "<tr class='#{prvclasses}' id='#{project.parent_id.to_s + "span"}' ><td class='name' >"
          output << "<span style='padding-left: " + (2*(ancestors.length-1)).to_s + "em;'></span>"
          output << "<span " + openonclick + " class='expander'>&nbsp; </span>"
          output << h("<Private Project>") + "<span #{openonclick} class='empty'>&nbsp; </span></td>"
          output << "<td #{openonclick} >Project is private.</td></tr>"
          ancestors << project.parent_id
        end
        classes << " hide #{ancestors.join(' ')}"
        ancestors << project.id
      end
      output << "<tr class='#{classes}' #{rowid}>"
      output << "<td class='name'><span style='padding-left: #{(2*(ancestors.length-1)).to_s}em;'></span> #{spanicon}"
      if project.active?
        output << link_to(h(project.name), {:controller => 'projects', :action => 'show', :id => project}, :class => "project")
      else
        output << h(project.name)
      end
      output << "<span #{openonclick} class='empty #{member_projects.include?(project) ? 'my-project' : nil}'>&nbsp; </span></td>"
      output << "<td #{openonclick} >#{textilizable project.short_description.gsub(/\!.+\!/,""), :project => project}</td></tr>"
    end
    return output
  end
end